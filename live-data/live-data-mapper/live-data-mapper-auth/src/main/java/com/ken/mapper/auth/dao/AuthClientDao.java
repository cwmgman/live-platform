package com.ken.mapper.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ken.entity.auth.AuthClient;

/**
 * 认证客户端表(AuthClient)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-12 12:03:33
 */
public interface AuthClientDao extends BaseMapper<AuthClient> {

}
