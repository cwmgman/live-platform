package com.ken.mapper.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ken.entity.auth.AuthRolePowerRelation;

/**
 * 角色权限关联表(AuthRolePowerRelation)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-12 12:03:36
 */
public interface AuthRolePowerRelationDao extends BaseMapper<AuthRolePowerRelation> {

}
