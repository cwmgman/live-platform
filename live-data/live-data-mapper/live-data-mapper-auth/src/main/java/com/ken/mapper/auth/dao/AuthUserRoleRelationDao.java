package com.ken.mapper.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ken.entity.auth.AuthUserRoleRelation;

/**
 * 用户角色关联表(AuthUserRoleRelation)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-12 12:03:36
 */
public interface AuthUserRoleRelationDao extends BaseMapper<AuthUserRoleRelation> {

}
