package com.ken.mapper.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ken.entity.auth.AuthRole;

/**
 * 角色表(AuthRole)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-12 12:03:36
 */
public interface AuthRoleDao extends BaseMapper<AuthRole> {

}
