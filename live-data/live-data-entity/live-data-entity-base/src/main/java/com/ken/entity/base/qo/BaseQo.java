package com.ken.entity.base.qo;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseQo implements Serializable {

    //状态位
    private Integer status = 0;

    //删除标识
    private Integer delFlag = 0;
}
