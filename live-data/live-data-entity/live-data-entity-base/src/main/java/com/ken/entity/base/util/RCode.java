package com.ken.entity.base.util;

/**
 * 返回码
 */
public enum RCode {

    SUCCESS(200, "操作成功"),

    HANDLE_FAIL(101, "操作失败"),

    VALIDATE_CODE_FAIL(401, "验证码异常"),

    AUTH_INVALID(402, "身份认证失败"),

    USER_RIGHTS_FAIL(403, "用户权限不够"),

    FAIL(500, "服务器异常，请稍后再试"),

    PARAM_INVALID(501, "参数不合法"),

    ORIGIN_INVALID(502, "接口请求来源不合法"),

    TOO_FREQUENTLY(503, "接口请求太频繁"),

    VERSION_INVALID(504, "版本不匹配"),

    PERSISTENCE_FAIL(506, "持久层异常"),

    RESOURCE_NOT_FOUND(507, "资源不存在"),

    ENCODING_FAIL(508, "编码处理异常"),

    SIGN_FAIL(509, "签名验签发生异常"),

    VERIFY_INVALID(510, "验证数据不合法"),

    SERVICE_NOT_FOUND(513, "服务不存在"),

    MQ_SEND_FAIL(514, "MQ消息发送不成功"),

    SMS_SEND_FAIL(515, "短信发送不成功"),

    DEL_CACHE_FAIL(516, "删除缓存不成功"),

    ID_GENERATOR_FAIL(517, "ID生成失败"),

    DISTRIBUTED_LOCK(519, "分布式锁占用中"),

    SCHEDULE_FAIL(530, "任务调度失败"),

    GATEWAY_FALL_BACK(543, "网关触发降级"),

    SERVER_FALL_BACK(542, "服务已经降级"),

    TOKEN_FAIL(600, "Token失效");



    public Integer code;

    public String message;

    RCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
