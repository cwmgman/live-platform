package com.ken.ability.sentinel.application;

import com.alibaba.csp.sentinel.cluster.server.ClusterTokenServer;
import com.alibaba.csp.sentinel.cluster.server.SentinelDefaultTokenServer;
import com.alibaba.csp.sentinel.cluster.server.config.ClusterServerConfigManager;
import com.alibaba.csp.sentinel.cluster.server.config.ServerTransportConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Sentinel集群流控Server端 初始化配置
 */
@Component
public class SentinelClusterInit implements CommandLineRunner {


    @Value("${server.port}")
    private Integer port;

    @Override
    public void run(String... args) throws Exception {
        // 加载namespace - 命名空间
//        ClusterServerConfigManager.loadServerNamespaceSet(Collections.singleton(appName));
        // 加载服务配置
        ClusterServerConfigManager.loadGlobalTransportConfig(new ServerTransportConfig()
                .setIdleSeconds(600)
                .setPort(port));

        //启动服务
        ClusterTokenServer tokenServer = new SentinelDefaultTokenServer();
        tokenServer.start();
    }


}
