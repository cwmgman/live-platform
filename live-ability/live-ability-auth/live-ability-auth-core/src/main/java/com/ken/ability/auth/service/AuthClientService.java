package com.ken.ability.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ken.entity.auth.AuthClient;

/**
 * 认证客户端表(AuthClient)表服务接口
 *
 * @author makejava
 * @since 2021-09-12 11:12:28
 */
public interface AuthClientService extends IService<AuthClient> {

}
