package com.ken.ability.auth.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ken.ability.auth.service.AuthPowerService;
import com.ken.entity.auth.AuthPower;
import com.ken.mapper.auth.dao.AuthPowerDao;
import org.springframework.stereotype.Service;

/**
 * 权限表(AuthPower)表服务实现类
 *
 * @author makejava
 * @since 2021-09-12 12:29:37
 */
@Service
public class AuthPowerServiceImpl extends ServiceImpl<AuthPowerDao, AuthPower> implements AuthPowerService {

}
