package com.ken.common.web.valid.annotation.constraint;

import com.ken.common.core.utils.ApplicationContextUtils;
import com.ken.common.web.valid.annotation.KenValid;
import com.ken.common.web.valid.extend.ValidHandler;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 自定义JSR303 校验约束对象
 */
public class KenValidConstraint implements ConstraintValidator<KenValid, Object> {

    private KenValid kenValid;

    @Override
    public void initialize(KenValid kenValid) {
        this.kenValid = kenValid;
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext constraintValidatorContext) {
        //获得需要执行的校验器
        ValidHandler validHandler = ApplicationContextUtils.getBean(kenValid.handler());
        //执行校验器校验
        boolean result = validHandler.handler(kenValid, obj);
        //返回结果
        return result;
    }
}
