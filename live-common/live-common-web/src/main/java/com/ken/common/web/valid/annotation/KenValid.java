package com.ken.common.web.valid.annotation;

import com.ken.common.web.valid.annotation.constraint.KenValidConstraint;
import com.ken.common.web.valid.extend.ValidHandler;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义的入参校验注解
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
//指定注解验证的逻辑实现类
@Constraint(validatedBy = KenValidConstraint.class)
public @interface KenValid {

    String message() default "校验失败";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    /**
     * 指定校验的实际处理器
     * @return
     */
    Class<? extends ValidHandler> handler();
}
