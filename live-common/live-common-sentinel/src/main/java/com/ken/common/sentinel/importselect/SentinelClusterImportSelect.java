package com.ken.common.sentinel.importselect;

import com.ken.common.sentinel.annotation.EnableSentinelCluster;
import com.ken.common.sentinel.applicaiton.SentinelClusterClientConfiguration;
import com.ken.common.sentinel.applicaiton.SentinelClusterServerConfiguration;
import com.ken.common.sentinel.applicaiton.SentinelGatewayConfiguration;
import com.ken.common.sentinel.config.SentinelClusterStatus;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;

/**
 * 导入类的选择器
 * 根据@EnableSentinelCluster启用集群流控的服务端或者客户端
 */
public class SentinelClusterImportSelect implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        Map<String, Object> annotationAttributes = importingClassMetadata.getAnnotationAttributes(EnableSentinelCluster.class.getName());
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(annotationAttributes);
        //获得注解属性
        SentinelClusterStatus clusterStatus = attributes.getEnum("value");
        if(clusterStatus == SentinelClusterStatus.SERVER) {
            return new String[]{SentinelClusterServerConfiguration.class.getName()};
        } else if(clusterStatus == SentinelClusterStatus.CLIENT) {
            return new String[]{SentinelClusterClientConfiguration.class.getName()};
        }
        return new String[]{SentinelGatewayConfiguration.class.getName()};
    }
}
