package com.ken.common.sentinel.config;

public enum SentinelClusterStatus {

    SERVER,
    CLIENT,
    GATEWAY;
}
