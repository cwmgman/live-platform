package com.ken.common.sentinel.annotation;

import com.ken.common.sentinel.config.SentinelClusterStatus;
import com.ken.common.sentinel.config.SentinelNacosProperties;
import com.ken.common.sentinel.importselect.SentinelClusterImportSelect;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启客户端集群流控功能
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({SentinelClusterImportSelect.class})
@EnableConfigurationProperties(SentinelNacosProperties.class)
public @interface EnableSentinelCluster {

    //启用的类型，默认客户端
    SentinelClusterStatus value() default SentinelClusterStatus.CLIENT;
}
