package com.ken.common.event.framework.message;

import com.ken.common.core.utils.ApplicationContextUtils;
import com.ken.common.event.framework.generate.MsgIdGenerate;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 事件消息封装对象
 */
@Data
public class EventMessage<T> implements Serializable {

    /**
     * 消息Id
     */
    private String msgId;

    /**
     * 消息的创建时间
     */
    private Date createTime;

    /**
     * 消息对象
     */
    private T msg;

    /**
     * 构造方法
     */
    public EventMessage(){
        //创建时间
        this.createTime = new Date();
    }

    /**
     * 有参构造方法
     * @param msg
     */
    public EventMessage(T msg) {
        this();
        this.setMsg(msg);
    }

    public void setMsg(T msg) {
        this.msg = msg;
        MsgIdGenerate idGenerate = ApplicationContextUtils.getBean(MsgIdGenerate.class);
        this.msgId = idGenerate.generate(msg);
    }
}
