package com.ken.common.event.core.rabbitmq.constact;

public interface RabbitMqConstact {

    String EVENT_EXCHANGE_DELAY = "event-exchange-delay";

    String EVENT_EXCHANGE = "event-exchange";

    String EVENT_QUEUE_DELAY_PRFIX = "event-queue-delay-";

    String EVENT_QUEUE_PRFIX = "event-queue-";
}
