package com.ken.common.event.framework.processor;


import com.ken.common.event.apply.handle.EventHandler;
import com.ken.common.event.framework.message.EventMessage;

/**
 * 事件消息消费前后处理器
 */
public interface EventMsgPostProcessor {

    default boolean isSupport(String eventTypeStr, EventMessage eventMessage, EventHandler eventHandler){
        return true;
    }

    default boolean beginProcessor(EventMessage eventMessage, EventHandler eventHandler){
        return true;
    }

    default boolean afterProcessor(EventMessage eventMessage, EventHandler eventHandler, Throwable e){
        return true;
    }
}
