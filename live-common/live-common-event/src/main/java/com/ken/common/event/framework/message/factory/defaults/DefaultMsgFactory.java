package com.ken.common.event.framework.message.factory.defaults;

import com.ken.common.event.framework.message.EventMessage;
import com.ken.common.event.framework.message.factory.MsgFactory;

/**
 * 默认的消息工厂类
 */
public class DefaultMsgFactory implements MsgFactory {
    @Override
    public <T> EventMessage<T> createMessage(T msg) {
        return new EventMessage<>(msg);
    }
}
