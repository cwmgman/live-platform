package com.ken.common.event.framework.generate;

/**
 * msgid生成器
 */
public interface MsgIdGenerate {

    <T> String generate(T msg);
}
