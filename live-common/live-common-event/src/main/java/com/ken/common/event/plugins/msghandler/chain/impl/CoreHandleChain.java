package com.ken.common.event.plugins.msghandler.chain.impl;

import com.ken.common.event.apply.handle.EventHandler;
import com.ken.common.event.framework.message.EventMessage;
import com.ken.common.event.plugins.msghandler.chain.AbstractMsgChain;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 核心的业务执行器
 */
@Component
@Order(50)
@Slf4j
public class CoreHandleChain extends AbstractMsgChain<EventHandler> {

    @Override
    public void coreRun(ChainContext context, EventHandler eventHandler) {

        //获得消息的实际消息体对象
        EventMessage eventMessage = context.getEventMessage();
        Object msgBody = eventMessage.getMsg();
        //执行核心业务
        eventHandler.eventHandler(msgBody, eventMessage);

        //往后继续透传
        super.nextInvoce(context,eventHandler);
    }
}
