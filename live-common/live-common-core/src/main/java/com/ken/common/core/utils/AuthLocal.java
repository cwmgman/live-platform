package com.ken.common.core.utils;

import com.ken.entity.auth.AuthUser;

/**
 * 当前用户持久对象
 */
public class AuthLocal {

    private static ThreadLocal<AuthUser> authUserThreadLocal = new ThreadLocal<>();

    public static void setUser(AuthUser authUser){
        AuthLocal.authUserThreadLocal.set(authUser);
    }

    public static AuthUser getUser(){
        return AuthLocal.authUserThreadLocal.get();
    }

    public static void clear(){
        AuthLocal.authUserThreadLocal.remove();
    }
}
