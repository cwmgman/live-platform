package com.ken.common.cache.handler;

import com.ken.common.event.apply.handle.EventHandler;
import com.ken.common.event.apply.handle.annotation.EventType;
import com.ken.common.event.framework.message.EventMessage;
import org.springframework.beans.factory.annotation.Autowired;

@EventType("cache-delete")
public class MemoryCacheEventHandler implements EventHandler<String> {

    @Autowired
    private MemoryCacheHandler memoryCacheHandler;

    @Override
    public void eventHandler(String key, EventMessage eventMessage) {
        System.out.println("接收到删除key：" + key);
        memoryCacheHandler.deleteCache(key);
    }
}
